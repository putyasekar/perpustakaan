<?php

namespace App\Http\Controllers\dash;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\Borrow;
use App\Models\Book;

class DashboardController extends Controller
{
    
    public function count()
    {
    	$Genre = Genre::all()->count();
    	$Book = Book::all()->count();
    	$Borrow = Borrow::all()->count();

    	return view('dash.index', compact('Genre', 'Book', 'Borrow'));
    }
}
