<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index()
    {
       $User = User::all();

       return view ('user.index',compact('User'));
    }
    
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required'
        ]);

        try{
            $User = new User();
            $User->name = $request->name;
            $User->address = $request->address;
            $User->save();

            Session::flash('message', 'Data Berhasil Disimpan');
            return redirect()->back();
        } catch (Exception $e){
            Session::flash('message', 'Data Tidak Berhasil Disimpan');
            return redirect()->back;
        }
    }
}
