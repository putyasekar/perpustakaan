<?php

namespace App\Http\Controllers\Genre;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Genre;
use Illuminate\Support\Facades\Session;

class GenreController extends Controller
{
    public function index()
    {
       $Genre = Genre::orderBy('created_at', 'desc')->paginate(5);

       return view ('genre.index',compact('Genre'));
    }
    
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required'
    	]);

    	try{
    		$Genre = new Genre();
    		$Genre->name = $request->name;
    		$Genre->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e){
    		Session::flash('message', 'Data Tidak Berhasil Disimpan');
    		return redirect()->back;
    	}
    }

    public function update( Request $request, $id)
    {
        $Genre = Genre::all();
        $Genre->id = $request->id;
        $Genre->name = $request->name;
        
        $Genre->save();

        Session::flash('message', 'Berhasil Update');

        return redirect()->back();
    }

    public function delete($id)
    {
        $Genre = Genre::find($id);

        $Genre->delete();

        Session::flash('message', 'Berhasil Menghapus');

        return redirect()->back();
    }

    public function detail($id)
    {
        $Genre = Genre::find($id);

        return view('genre.detail', compact('Genre'));
    }
}
