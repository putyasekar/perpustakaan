<?php

namespace App\Http\Controllers\Peminjaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Borrow;
use App\Models\Books;
use App\User;
use Illuminate\Support\Facades\Session;

class PeminjamanController extends Controller
{
    public function index()
    {
        $User = User::all();
       $Borrows = Borrow::orderBy('created_at', 'desc')->paginate(5);

       $Book = Books::select(['id', 'judul'])->orderBy('judul')->get();
       return view ('peminjaman.index',compact('Borrows', 'User', 'Book'));
    }
     public function store(Request $request)
    {
        $this->validate($request,[
            'user_id' => 'required',
            'books_id' => 'required',
            'tanggal_peminjaman' => 'required',
            'tanggal_pengembalian' => 'required'
        ]);

        try{
            $Peminjaman = new Borrow();
            $Peminjaman->user_id = $request->user_id;
            $Peminjaman->books_id = $request->books_id;
            $Peminjaman->tanggal_peminjaman = $request->tanggal_peminjaman;
            $Peminjaman->tanggal_pengembalian = $request->tanggal_pengembalian;
            $Peminjaman->save();

            Session::flash('message', 'Data Berhasil Disimpan');
            return redirect()->back();
        } catch (Exception $e){
            Session::flash('message', 'Data Tidak Berhasil Disimpan');
            return redirect()->back;
        }
    }

    public function update( Request $request, $id)
    {
        $Peminjaman = Borrow::all();
        $Peminjaman->id = $request->id;
        $Peminjaman->user_id = $request->user_id;
        $Peminjaman->books_id = $request->books_id;
        $Peminjaman->tanggal_peminjaman = $request->tanggal_peminjaman;
        $Peminjaman->tanggal_pengembalian = $request->tanggal_pengembalian;
        
        $Peminjaman->save();

        Session::flash('message', 'Berhasil Update');

        return redirect()->back();
    }

    public function delete($id)
    {
        $Peminjaman = Borrow::find($id);

        $Peminjaman->delete();

        Session::flash('message', 'Berhasil Menghapus');

        return redirect()->back();
    }
}
