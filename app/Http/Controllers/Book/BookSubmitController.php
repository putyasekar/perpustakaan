<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Books;
use App\Models\Genre;
use Illuminate\Support\Facades\Session;

class BookSubmitController extends Controller
{
    public function index()
    {
        //search
        $Book = Books::orderBy('created_at', 'desc')->paginate(5);

        $Genre = Genre::select(['id', 'name'])->orderBy('name')->get();
        return view('book.index', compact('Book', 'Genre'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'genre_id' => 'required'
        ]);

        try{
            $Book = new Books();
            $Book->judul = $request->judul;
            $Book->penulis = $request->penulis;
            $Book->penerbit = $request->penerbit;
            $Book->genre_id = $request->genre_id;
            $Book->save();

            Session::flash('message', 'Data Berhasil Disimpan');
            return redirect()->back();
        } catch (Exception $e){
            Session::flash('message', 'Data Tidak Berhasil Disimpan');
            return redirect()->back();
        }
    }

    public function search_book(Request $request)
    {
        $Book = Books::query();
        $Genre = Genre::query();
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $Book->where("judul", "like", "%" . request()->query("search") . "%");
        }

        $pagination = 5;
        $Book = $Book->orderBy('created_at', 'desc')->paginate($pagination);

        return view('book.index', compact('Book', 'Genre'));
    }

    public function filter_book(Request $request)
    {
        $pagination = 5;
        $Genre = Genre::all();
        $Book = Books::whereDate('created_at', '=', $request->created_at)->orderBy('created_at', 'desc')->paginate($pagination);

        return view('book.index', compact('Book', 'Genre'));
    }

    public function edit($id)
    {
        $Genre = Genre::all();
        $Book = Books::find($id);

        return view('book.edit', compact('Book', 'Genre'));
    }

    public function update( Request $request, $id)
    {
        $Genre = Genre::all();
        $Book = Books::find($id);
        $Book->judul = $request->judul;
        $Book->penulis = $request->penulis;
        $Book->penerbit = $request->penerbit;
        $Book->genre_id = $request->genre_id;

        $Book->save();

        Session::flash('message', 'Berhasil update');

        return redirect()->back();
    }

    public function delete($id)
    {
        $Book = Books::find($id);

        $Book->delete();

        Session::flash('message', 'Berhasil Menghapus');

        return redirect()->back();
    }

    public function detail($id)
    {
        $Genre = Genre::all();
        $Book = Books::find($id);

        return view('book.detail', compact('Book', 'Genre'));
    }
}