<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Validation\ValidationException;
use App\Models\Borrow;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Borrow = Borrow::all();

            $response=$Borrow;
            $code=200;
        }catch(Exception $e) {
            $code=500;
            $response=$e->getMessage();
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'books_id' => 'required',
            'tanggal_peminjaman' => 'required',
            'tanggal_pengembalian' => 'required',
        ]);

        try {
            $Borrow = new Borrow();
            $Borrow->user_id = $request->user_id;
            $Borrow->books_id = $request->books_id;
            $Borrow->tanggal_peminjaman = $request->tanggal_peminjaman;
            $Borrow->tanggal_pengembalian = $request->tanggal_pengembalian;

            $Borrow->save();

            $code = 200;
            $response = $Borrow;

        } catch (Exception $e) {
            if ($e instanceof ValidationException) {
                $code = 404;
                $response = 'tidak ada data';
            } else {
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         try {
        $Borrow = Borrow::findOrFail($id);

        $code = 200;
        $response = $Borrow;
    } catch (\Exception $e){
        if ($e instanceof ModelNotFoundException) {
            $code = 404;
            $response = 'inputkan sesuai id';
        }else{
            $code = 500;
            $response = $e->getMessage();
        }
    }
    return apiResponseBuilder($code, $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Borrow = Borrow::find($id);
            $Borrow->delete();

            $code = 200;
            $response = $Borrow;
        } catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage;
        }

        return apiResponseBuilder($code, $response);
    
    }
}
