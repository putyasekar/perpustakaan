<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Books;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Buku = Books::all();

            $response=$Buku;
            $code=200;
        }catch(Exception $e) {
            $code=500;
            $response=$e->getMessage();
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'genre_id' => 'required',
        ]);

        try{
            $Buku = new Books();
            $Buku->judul = $request->judul;
            $Buku->penulis = $request->penulis;
            $Buku->penerbit = $request->penerbit;
            $Buku->genre_id = $request->genre_id;

            $Buku->save();

            $code = 200;
            $response = $Buku;
            
        }catch (\Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
        $Buku = Books::findOrFail($id);

        $code = 200;
        $response = $Buku;
    } catch (\Exception $e){
        if ($e instanceof ModelNotFoundException) {
            $code = 404;
            $response = 'inputkan sesuai id';
        }else{
            $code = 500;
            $response = $e->getMessage();
        }
    }
    return apiResponseBuilder($code, $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'genre_id' => 'required',

        ]);

        try {
            $Buku = Books::find($id);

            $Buku->judul = $request->judul;
            $Buku->penulis = $request->penulis;
            $Buku->penerbit = $request->penerbit;
            $Buku->genre_id = $request->genre_id;
            $Buku->save();
            $code = 200;
            $response = $Buku;
        } catch (\Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'data tidak ada';
            } else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Buku = Books::find($id);
            $Buku->delete();

            $code = 200;
            $response = $Buku;
        } catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage;
        }

        return apiResponseBuilder($code, $response);
    
    }
}
