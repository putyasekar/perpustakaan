<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Validation\ValidationException;
use App\Models\Genre;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         try {
            $Genre = Genre::all();

            $response=$Genre;
            $code=200;
        }catch(Exception $e) {
            $code=500;
            $response=$e->getMessage();
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required | min:3 | max:20',
        ]);

        try {
            $Genre = new Genre();
            $Genre->name = $request->name;

            $Genre->save();

            $code = 200;
            $response = $Genre;

        } catch (Exception $e) {
            if ($e instanceof ValidationException) {
                $code = 404;
                $response = 'tidak ada data';
            } else {
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
        $Genre = Genre::findOrFail($id);

        $code = 200;
        $response = $Genre;
    } catch (\Exception $e){
        if ($e instanceof ModelNotFoundException) {
            $code = 404;
            $response = 'inputkan sesuai id';
        }else{
            $code = 500;
            $response = $e->getMessage();
        }
    }
    return apiResponseBuilder($code, $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name' => 'required',

        ]);

        try {
            $Genre = Genre::find($id);

            $Genre->name = $request->name;
            $Genre->save();
            $code = 200;
            $response = $Genre;
        } catch (\Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'data tidak ada';
            } else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try {
            $Genre = Genre::find($id);
            $Genre->delete();

            $code = 200;
            $response = $Genre;
        } catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);

    }
}
