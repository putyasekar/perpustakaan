<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'name' => 'required',
    		'address' => 'required ',
    		'image' => 'required'
    	]);
    	if ($validator->fails()) {
    		return apiResponseValidationFails('Validation Error Message', $validator->errors()->all());
    	}

    	$imageName = time().'.'.request()->image->getClientOriginalExtension();

    	request()->image->move(public_path('images'), $imageName);

    	// $user = User::create([
    	// 	'name' => $request->name,
    	// 	'email' => $request->email,
    	// 	'image' => $request->image
    	// ]);

    	$user = new User();
    	$user->name = $request->name;
    	$user->address = $request->address;
    	$user->image = $imageName;
    	$user->save();

    	$success['user'] = $user;

    	return apiResponseSuccess('Register Success!', $success, 200);
    }
}
