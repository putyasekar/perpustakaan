<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Books extends Model
{
    protected $table = 'books';

    use softDeletes;

    public function Borrow()
    {
    	return $this->hasOne(Borrow::class, 'books_id', 'id');
    }
}
