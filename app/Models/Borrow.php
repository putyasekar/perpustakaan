<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Borrow extends Model
{
     protected $table = 'borrow';

    use softDeletes;

    public function User()
    {
    	return $this->hasMany(User::class, 'user_id', 'id');
    }
    public function Books()
    {
    	return $this->hasMany(Books::class, 'books_id', 'id');
    }
}
