<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Genre extends Model
{
    protected $table = 'genre';

    use softDeletes;

    public function Books()
    {
    	return $this->hasMany(Books::class, 'genre_id', 'id');
    }
}
