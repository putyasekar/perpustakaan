<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dash.index');
});

//genre
Route::post('/genre/store', 'Genre\GenreController@store')->name('genre.store');
Route::get('/genre', 'Genre\GenreController@index');
Route::get('/genre/{id}/delete', 'Genre\GenreController@delete');
Route::post('/genre/{id}/update', 'Genre\GenreController@update');
Route::get('/genre/{id}', 'Genre\GenreController@detail');

//book
Route::post('/book/store', 'Book\BookSubmitController@store')->name('book.store');
Route::get('/book', 'Book\BookSubmitController@index');
Route::get('/book/{id}/edit', 'Book\BookSubmitController@edit');
Route::get('/book/{id}/delete', 'Book\BookSubmitController@delete');
Route::post('/book/{id}/update', 'Book\BookSubmitController@update');
Route::get('/search_book', 'Book\BookSubmitController@search_book')->name('search_book');
Route::get('/filter_book', 'Book\BookSubmitController@filter_book')->name('filter_book');
Route::get('/book/{id}', 'Book\BookSubmitController@detail');

//peminjaman
Route::post('/peminjaman/store', 'Peminjaman\PeminjamanController@store')->name('peminjaman.store');
Route::get('/peminjaman', 'Peminjaman\PeminjamanController@index');
Route::get('/peminjaman/{id}/delete', 'Peminjaman\PeminjamanController@delete');
Route::post('/peminjaman/{id}/update', 'Peminjaman\PeminjamanController@update');
Route::get('/peminjaman/{id}', 'Peminjaman\PeminjamanController@detail');

//user
Route::post('/user/store', 'User\UserController@store')->name('user.store');
Route::get('/user', 'User\UserController@index');

//dashboard
Route::get('/count', 'dash\DashboardController@count')->name('count');