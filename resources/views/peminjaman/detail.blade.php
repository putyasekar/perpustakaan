@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Peminjaman</h1>
	</section>
  
  <section class="content">
    
		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Peminjaman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form role="form" action="/peminjaman/{{$Peminjaman->id}}" method="get" enctype="multipart/form-data">
                @csrf

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <table class="table table-bordered">
                  <tr>
                    <td>Id</td>
                    <td>{{ $Peminjaman->id }}</td>
                  </tr>
                  <tr>
                    <td>User ID</td>
                    <td>{{ $Peminjaman->user_id }}</td>
                  </tr>
                  <tr>
                    <td>Books ID</td>
                    <td>{{ $Peminjaman->books_id }}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Peminjaman</td>
                    <td>{{ $Peminjaman->tanggal_peminjaman }}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Pengembalian</td>
                    <td>{{ $Peminjaman->tanggal_pengembalian }}</td>
                  </tr>
                </table>
                  <div class="form-group">
              <a class="btn btn-warning" href="/peminjaman">Back</a>
            </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
	</section>
</div>

@include('base.footer')