@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Peminjaman</h1>
	</section>
  
  <section class="content">
		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Insert For The Borrower</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form action="/peminjaman/store" method="POST">
                @csrf

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <div class="form-group">
              <label for="exampleInputEmail">User ID</label>
              <select class="form-control" name="user_id" required>
                <option> -- Pilih Salah Satu --</option>
                @foreach($User as $user)
                <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
              </select>
            </div>

                 <div class="form-group">
              <label for="exampleInputEmail">Book ID</label>
              <select class="form-control" name="books_id" required>
                <option> -- Pilih Salah Satu --</option>
                @foreach($Book as $book)
                <option value="{{$book->id}}">{{$book->judul}}</option>
                @endforeach
              </select>
            </div>

                 <div class="form-group">
                  <label>tanggal_peminjaman</label>
                  <input type="Date" class="form-control" name="tanggal_peminjaman" placeholder="Enter The Borrow Date">
                </div>

                <div class="form-group">
                  <label>tanggal_pengembalian</label>
                  <input type="Date" class="form-control" name="tanggal_pengembalian" placeholder="Enter The Return Date">
                </div>

                <input type="submit" name="Add">
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Borrow Table</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form action="/peminjaman" method="GET">
              </form>
              <table class="table table-hover">
                <tr>
                  <th>id</th>
                  <th>user_id</th>
                  <th>books_id</th>
                  <th>tanggal_peminjaman</th>
                  <th>tanggal_pengembalian</th>
                  <th>action</th>
                </tr>

                @foreach($Borrows as $item)
                <tr>
                  <td>{{$item->id}}</td>
                  <td>{{$item->user_id}}</td>
                  <td>{{$item->books_id}}</td>
                  <td>{{$item->tanggal_peminjaman}}</td>
                  <td>{{$item->tanggal_pengembalian}}</td>
                  <td>
                    <a class="btn btn-success btn-sm" href="/peminjaman/{{$item->id}}/edit">Update</a>
                    <a class="btn btn-danger btn-sm" href="/peminjaman/{{$item->id}}/delete">Delete</a>
                    <a class="btn btn-primary btn-sm" href="/peminjaman/{{$item->id}}">Detail</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>

            <div class="text-center">
              {{ $Borrows->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

	</section>
</div>

@include('base.footer')