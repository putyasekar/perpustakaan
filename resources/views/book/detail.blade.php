@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Book</h1>
	</section>
  
  <section class="content">
    
		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Book</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form role="form" action="/book/{{$Book->id}}" method="get" enctype="multipart/form-data">
                @csrf

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <table class="table table-bordered">
                  <tr>
                    <td>Id</td>
                    <td>{{ $Book->id }}</td>
                  </tr>
                  <tr>
                    <td>Judul</td>
                    <td>{{ $Book->judul }}</td>
                  </tr>
                  <tr>
                    <td>Penulis</td>
                    <td>{{ $Book->penulis }}</td>
                  </tr>
                  <tr>
                    <td>Penerbit</td>
                    <td>{{ $Book->penerbit }}</td>
                  </tr>
                  <tr>
                    <td>Genre</td>
                    <td>{{ $Book->genre['name'] }}</td>
                  </tr>
                </table>
                  <div class="form-group">
              <a class="btn btn-warning" href="/book">Back</a>
            </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
	</section>
</div>

@include('base.footer')