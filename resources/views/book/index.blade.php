@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Book</h1>
	</section>
  
  <section class="content">
    <div class="box-body">
      <form action="{{ route('filter_book') }}" method="get" class="form-group">
        @csrf
        <label>PILIH TANGGAL</label>
        <input type="date" name="created-at">
        <input type="submit" name="FILTER">
      </form>
    </div>
    
		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Insert Book Items</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form action="/book/store" method="POST">
                @csrf

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label>Book Title</label>
                  <input type="text" class="form-control" name="judul" placeholder="Enter The Title ...">
                </div>

                 <div class="form-group">
                  <label>Writer Name</label>
                  <input type="text" class="form-control" name="penulis" placeholder="Enter Writer Name ...">
                </div>

                 <div class="form-group">
                  <label>Publisher Name</label>
                  <input type="text" class="form-control" name="penerbit" placeholder="Enter The Publisher ...">
                </div>

                 
            <div class="form-group">
              <label for="exampleInputEmail">Genre</label>
              <select class="form-control" name="genre_id" required>
                <option> -- Pilih Salah Satu --</option>
                @foreach($Genre as $genre)
                <option value="{{$genre->id}}">{{$genre->name}}</option>
                @endforeach
              </select>
            </div>
                
                <input type="submit" name="Add">
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Genre Table</h3>

              <div class="box-tools">
                <form action="{{ route('search_book') }}" method="GET" class="form-group">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form action="/book" method="GET">
              </form>
              <table class="table table-hover">
                <tr>
                  <th>id</th>
                  <th>judul</th>
                  <th>penulis</th>
                  <th>penerbit</th>
                  <th>genre</th>
                  <th>action</th>
                </tr>

                @foreach($Book as $item)
                <tr>
                  <td>{{$item->id}}</td>
                  <td>{{$item->judul}}</td>
                  <td>{{$item->penulis}}</td>
                  <td>{{$item->penerbit}}</td>
                  <td>{{$item->genre['name'] }}</td>
                  <td>
                    <a class="btn btn-success btn-sm" href="/book/{{$item->id}}/edit">Update</a>
                    <a class="btn btn-danger btn-sm" href="/book/{{$item->id}}/delete">Delete</a>
                    <a class="btn btn-primary btn-sm" href="/book/{{$item->id}}">Detail</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>

            <div class="text-center">
              {{ $Book->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
	</section>
</div>

@include('base.footer')