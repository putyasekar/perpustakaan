@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Genre</h1>
	</section>
  
  <section class="content">
    
		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Genre</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form role="form" action="/genre/{{$Genre->id}}/update" method="post" enctype="multipart/form-data">
                @csrf

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label>ID</label>
                  <input type="text" class="form-control" name="id" value="{{ $Genre-> id }}">
                </div>

                 <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" name="name" value="{{ $Genre-> name }}">
                </div>

            <div class="form-group">
              <label for="exampleInputEmail">Genre</label>
              <select class="form-control" name="genre_id" required>
                @foreach($Genre as $genre)
                <option value="{{ $genre->id }}" {{($Genre->genre_id == $genre->id)?'selected':''}}>{{$genre->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <input class="btn btn-primary" type="submit" value="update"></input>
              <a class="btn btn-warning" href="/genre">Back</a>
            </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
	</section>
</div>

@include('base.footer')